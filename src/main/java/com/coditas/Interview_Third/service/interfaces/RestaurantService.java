package com.coditas.Interview_Third.service.interfaces;

import com.coditas.Interview_Third.entity.Food;
import com.coditas.Interview_Third.entity.Restaurant;
import com.coditas.Interview_Third.payload.FoodDto;
import com.coditas.Interview_Third.payload.RestaurantDto;

import java.util.List;

public interface RestaurantService {
    List<RestaurantDto> getAllRestaurant();
    List<FoodDto>  getAllFoodOfSpecificResto(long restaurantId);
}
