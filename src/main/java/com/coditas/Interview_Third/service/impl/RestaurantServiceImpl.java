package com.coditas.Interview_Third.service.impl;

import com.coditas.Interview_Third.entity.Food;
import com.coditas.Interview_Third.entity.Restaurant;
import com.coditas.Interview_Third.payload.FoodDto;
import com.coditas.Interview_Third.payload.RestaurantDto;
import com.coditas.Interview_Third.repository.RestaurantRepository;
import com.coditas.Interview_Third.service.interfaces.RestaurantService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RestaurantServiceImpl implements RestaurantService {
    @Autowired
    private RestaurantRepository restaurantRepository;
    @Override
    public List<RestaurantDto> getAllRestaurant() {
        List<Restaurant> list=restaurantRepository.findAll();
        List<Restaurant> sortedList=list.stream().sorted(Comparator.comparing(Restaurant::getRestaurantRating)).collect(Collectors.toList());
        List<RestaurantDto> restaurantDtoList=sortedList.stream().map(s->mapToRestaurantDto(s)).collect(Collectors.toList());
        return restaurantDtoList;
    }

    @Override
    public List<FoodDto> getAllFoodOfSpecificResto(long restaurantId) {
        Restaurant restaurant=restaurantRepository.findById(restaurantId).get();
        List<Food> foodList=restaurant.getFoodList();
        List<Food> sortedFoodList=foodList.stream().sorted(Comparator.comparing(Food::getNumberOfSales).reversed()).collect(Collectors.toList());
        List<FoodDto> foodDtoList=sortedFoodList.stream().map(f->mapToFoodDto(f)).collect(Collectors.toList());

        return foodDtoList;
    }

    public Restaurant mapToRestaurant(RestaurantDto restaurantDto){
        Restaurant restaurant=new Restaurant();
        restaurant.setRestaurantId(restaurantDto.getRestaurantId());
        restaurant.setRestaurantName(restaurantDto.getRestaurantName());
        restaurant.setRestaurantXPosition(restaurantDto.getRestaurantXPosition());
        restaurant.setRestaurantYPosition(restaurantDto.getRestaurantYPosition());
        restaurant.setRestaurantRating(restaurant.getRestaurantRating());
//        restaurant.setFoodList(restaurantDto.getFoodList());
        return restaurant;
    }

    public RestaurantDto mapToRestaurantDto(Restaurant restaurant){
        RestaurantDto restaurantDto=new RestaurantDto();
        restaurantDto.setRestaurantId(restaurant.getRestaurantId());
        restaurantDto.setRestaurantName(restaurant.getRestaurantName());
        restaurantDto.setRestaurantXPosition(restaurant.getRestaurantXPosition());
        restaurantDto.setRestaurantYPosition(restaurant.getRestaurantYPosition());
        restaurantDto.setRestaurantRating(restaurant.getRestaurantRating());
//        restaurantDto.setFoodList(restaurant.getFoodList());
        return restaurantDto;
    }

    public FoodDto mapToFoodDto(Food food){
        FoodDto foodDto=new FoodDto();
        foodDto.setFoodId(food.getFoodId());
        foodDto.setFoodName(food.getFoodName());
        foodDto.setNumberOfSales(food.getNumberOfSales());
        return foodDto;
    }
}
