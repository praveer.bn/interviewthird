package com.coditas.Interview_Third;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InterviewThirdApplication {

	public static void main(String[] args) {
		SpringApplication.run(InterviewThirdApplication.class, args);
	}

}
