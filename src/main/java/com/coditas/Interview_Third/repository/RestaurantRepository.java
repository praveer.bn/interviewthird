package com.coditas.Interview_Third.repository;

import com.coditas.Interview_Third.entity.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RestaurantRepository extends JpaRepository<Restaurant,Long> {
}
