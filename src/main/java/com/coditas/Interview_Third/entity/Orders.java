package com.coditas.Interview_Third.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Time;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long ordersId;
    private int distance;

    private Time estimatedTime;
    private String ordersStatus;

    @ManyToOne
    private User user;
    @ManyToOne
    private Restaurant restaurant;
    @ManyToOne
    private DeliveryPerson deliveryPerson;
    @ManyToMany
    private List<Food> foodList;



}
