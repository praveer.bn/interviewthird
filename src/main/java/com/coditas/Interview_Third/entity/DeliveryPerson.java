package com.coditas.Interview_Third.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class DeliveryPerson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long deliveryPersonId;
    private String deliveryPersonName;
    private int deliveryPersonXPosition;
    private int deliveryPersonYPosition;
    private boolean isAvailable=true;


    @OneToMany(mappedBy = "deliveryPerson",cascade = CascadeType.ALL)
    private List<Orders> ordersList;
}
