package com.coditas.Interview_Third.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Food {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long foodId;
    private String foodName;
    private int numberOfSales;

    @ManyToOne
    private Restaurant restaurant;

    @ManyToMany(mappedBy = "foodList")
    private List<Orders> ordersList;


}
