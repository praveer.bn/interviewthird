package com.coditas.Interview_Third.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Restaurant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long restaurantId;
    private String restaurantName;
    private int restaurantXPosition;
    private int restaurantYPosition;
    private float restaurantRating;

    @OneToMany(mappedBy = "restaurant",cascade = CascadeType.ALL)
    private List<Food> foodList;

    @OneToMany(mappedBy = "restaurant",cascade = CascadeType.ALL)
    private List<Orders> ordersList;
}
