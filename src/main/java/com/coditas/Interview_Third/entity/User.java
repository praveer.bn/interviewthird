package com.coditas.Interview_Third.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userId;
    private String userName;
    private int userXPosition;
    private int userYPosition;

    @OneToMany(mappedBy ="user",cascade = CascadeType.ALL)
    private List<Orders> ordersList;
}
