package com.coditas.Interview_Third.payload;

import com.coditas.Interview_Third.entity.Food;
import com.coditas.Interview_Third.entity.Orders;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RestaurantDto {
    private long restaurantId;
    private String restaurantName;
    private int restaurantXPosition;
    private int restaurantYPosition;
    private float restaurantRating;
    private List<Food> foodList;


}
