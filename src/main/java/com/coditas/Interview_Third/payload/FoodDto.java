package com.coditas.Interview_Third.payload;

import com.coditas.Interview_Third.entity.Orders;
import com.coditas.Interview_Third.entity.Restaurant;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.List;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FoodDto {
    private long foodId;
    private String foodName;
    private int numberOfSales;

}
