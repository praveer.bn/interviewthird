package com.coditas.Interview_Third.controller;

import com.coditas.Interview_Third.payload.FoodDto;
import com.coditas.Interview_Third.payload.RestaurantDto;
import com.coditas.Interview_Third.service.interfaces.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/resto")
public class RestaurantController {
    @Autowired
    public RestaurantService restaurantService;
    @GetMapping("/sort")
    public ResponseEntity<List<RestaurantDto>> getAllSortedRestaurant(){
        return new ResponseEntity<>(restaurantService.getAllRestaurant(), HttpStatus.OK);
    }
    @GetMapping("/restaurant/{restaurantId}")
    public ResponseEntity<List<FoodDto>> getAllSortedRestaurant(@PathVariable long restaurantId){
        return new ResponseEntity<>(restaurantService.getAllFoodOfSpecificResto(restaurantId), HttpStatus.OK);
    }

}
